use uom::si::f64::*;
use uom::si::angle::radian;
use uom::si::ratio::ratio;

fn ratio_to_radians(x: Ratio) -> Angle {
    Angle::new::<radian>(x.get::<ratio>())
}

pub fn find_true_anomaly(mean_anomaly: Angle, eccentricity: Ratio) -> Angle {
    let f = |x: Angle| { x - ratio_to_radians(eccentricity * x.sin()) - mean_anomaly };
    let df = |x: Angle| { Ratio::new::<ratio>(1.0) - eccentricity * x.cos() };
    
    let mut x = mean_anomaly;
    let epsilon = Angle::new::<radian>(1e-12);

    let mut v = f(x);

    while v.abs() > epsilon {
        x -= ratio_to_radians(v / df(x));
        v = f(x);
    }

    x
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f64::consts::*;
    use uom::si::f64::*;
    use uom::num::Zero;

    #[test]
    fn round() {
        for m in &[0.0, 0.1, 0.5, 0.8, 1.6, 2.0, 3.0, 3.14, 4.0, 5.0, 6.0, 6.28] {
            let mean_anomaly = Angle::new::<radian>(*m);
            let true_anomaly = find_true_anomaly(mean_anomaly, Ratio::zero());
            assert_eq!(true_anomaly, mean_anomaly);
        }
    }

    #[test]
    fn half_point() {
        for te in 0..10 {
            let e = te as f64 / 10.0;
            for ma in &[0.0, PI, TAU] {
                let mean_anomaly = Angle::new::<radian>(*ma);
                let true_anomaly = find_true_anomaly(mean_anomaly, Ratio::new::<ratio>(e));
                assert_eq!(true_anomaly, mean_anomaly);
            }
        }
    }

    #[test]
    fn not_equal() {
        for te in 1..10 {
            let e = te as f64 / 10.0;
            for ma in &[0.1, FRAC_PI_3, FRAC_PI_2, PI + FRAC_PI_2, 6.0] {
                let mean_anomaly = Angle::new::<radian>(*ma);
                let true_anomaly = find_true_anomaly(mean_anomaly, Ratio::new::<ratio>(e));
                assert_ne!(true_anomaly, mean_anomaly);
            }
        }
    }
}
