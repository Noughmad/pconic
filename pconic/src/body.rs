use std::fmt;
use std::path::Path;
use std::collections::HashMap;

use uom::si::f64::*;
use uom::si::angle::radian;
use uom::si::ratio::ratio;
use uom::si::mass::kilogram;
use uom::si::length::meter;
use uom::si::time::second;
use uom::typenum::{P3, N2};
use uom::num::Zero;
use daggy::{Dag, NodeIndex};
use serde::{Serialize, Deserialize};
use snafu::{Snafu, ResultExt, ensure};
use nalgebra::Vector3;

use crate::kepler::find_true_anomaly;

#[derive(Debug, Snafu)]
pub enum Error {
    SerdeError {
        #[snafu(source(from(serde_any::Error, failure::Fail::compat)))]
        source: failure::Compat<serde_any::Error>,
    },
    NonUniqueBodyName {
        name: String,
    },
    UnknownParentName {
        name: String,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CelestialBody {
    pub name: String,
    pub mass: Mass,
    pub radius: Length,
    pub atmosphere_height: Length,
    pub soi_radius: Length,
    pub orbit: Orbit,
    pub parent: Option<String>,
}

impl fmt::Display for CelestialBody {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct KeplerianElements {
    inclination: Angle,
    longitude_of_ascending_node: Angle,
    argument_of_periapsis: Angle,
    eccentricity: Ratio,
    semimajor_axis: Length,
    mean_anomaly: Angle,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct StateVectors {
    position: Vector3<Length>,
    velocity: Vector3<Velocity>,
}

impl From<&KeplerianElements> for StateVectors {
    fn from(ke: &KeplerianElements) -> Self {
        let one = Ratio::new::<ratio>(1.0);
        let gravitation = 6.67e-11f64 * Length::new::<meter>(1.0).powi(P3::new()) / Mass::new::<kilogram>(1.0) * Time::new::<second>(1.0).powi(N2::new());

        let eccentric_anomaly = find_true_anomaly(ke.mean_anomaly, ke.eccentricity);
        let (s, c) = (eccentric_anomaly / 2.0).sin_cos();
        let true_anomaly = 2.0 * ((one + ke.eccentricity).sqrt() * s).atan2((one - ke.eccentricity).sqrt() * s);

        let distance = ke.semimajor_axis * (one - ke.eccentricity * eccentric_anomaly.cos());

        let (sn, cn) = true_anomaly.sin_cos();

        let orbit_position = Vector3::<Length>::new(
            distance * cn,
            distance * sn,
            Length::zero(),
        );

        let mass = Mass::new::<kilogram>(1e23);

        let velocity = (mass * gravitation * ke.semimajor_axis).sqrt() / distance;
        let orbit_velocity = Vector3::<Velocity>::new(
            -velocity * s,
            velocity * (one - ke.eccentricity * ke.eccentricity).sqrt() * c,
            Velocity::zero(),
        );

        // TODO: Rotate both vectors from orbit system to a system centered on the body 

        StateVectors {
            position: orbit_position,
            velocity: orbit_velocity,
        }
    }
}

impl From<&StateVectors> for KeplerianElements {
    fn from(sv: &StateVectors) -> Self {
        Self::default()
    }
}
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Orbit {
    KeplerianElements(KeplerianElements),
    StateVectors(StateVectors),
}

impl Orbit {
    pub fn to_keplerian_elements(&self) -> KeplerianElements {
        match self {
            Orbit::KeplerianElements(ke) => ke.clone(),
            Orbit::StateVectors(sv) => sv.into(),
        }
    }
    
    pub fn to_state_vectors(&self) -> StateVectors {
        match self {
            Orbit::StateVectors(sv) => sv.clone(),
            Orbit::KeplerianElements(ke) => ke.into(),
        }
    }

    pub fn orbit_at(&self, time_angle: Angle) -> Orbit {
        let mut ke = self.to_keplerian_elements();
        ke.mean_anomaly += time_angle;
        Orbit::KeplerianElements(ke)
    }
}

#[derive(Debug)]
pub struct System {
    pub graph: Dag<CelestialBody, ()>,
    pub root: Option<NodeIndex>,
}

impl System {
    pub fn from_file(path: impl AsRef<Path>) -> Result<System, Error> {
        let bodies: Vec<CelestialBody> = serde_any::from_file(path).context(SerdeError)?;
        Self::from_bodies(bodies)
    }

    pub fn from_bodies(bodies: Vec<CelestialBody>) -> Result<System, Error> {
        let mut graph = Dag::<CelestialBody, ()>::new();
        let mut root = None;
        let mut named = HashMap::new();

        for body in bodies {
            ensure!(!named.contains_key(&body.name), NonUniqueBodyName {name: body.name});

            let n = body.name.clone();
            let i = if let Some(parent) = body.parent.clone() {
                ensure!(named.contains_key(&parent), UnknownParentName {name: parent});
                graph.add_child(named[&parent], (), body).1
            } else {
                graph.add_node(body)
            };
            named.insert(n, i);
            if root.is_none() {
                root = Some(i);
            }
        }

        Ok(System {
            graph,
            root,
        })
    }
}
