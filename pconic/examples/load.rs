use pconic::body::System;
use ptree::graph::print_graph;

fn main() {
    let system = System::from_file("pconic/examples/load.yaml").unwrap();
    print_graph(system.graph.graph(), system.root.unwrap()).unwrap();
}
